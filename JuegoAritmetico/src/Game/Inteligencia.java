package Game;

import java.util.ArrayList;


public class Inteligencia 
{
	private ArrayList<Integer> resultadosFilas;
	private ArrayList<Integer> resultadoColumnas;
	private int matriztablero[][];
	private int tamano;
	private int matrizresultado[][];
	private int tablerosACompletar;
	private int tablerosCompletados;
	
	public Inteligencia() 
	{
		setDificultadMedia();
		this.matriztablero=new int[this.tamano][this.tamano];
		this.matrizresultado=cargarMatrizResultado(this.tamano);
		this.resultadoColumnas=cargarResultadosColumnas();
		this.resultadosFilas=cargarResultadosFilas();

		

	}
	
	//Get y SET
	public int getResultadoFila(int fila)
	{
		noExcederDe(tamano, fila);
		noNegativos(fila);
		return resultadosFilas.get(fila);
	}

	public int getResultadoColumna(int columna){
		noExcederDe(tamano, columna);
		noNegativos(columna);
		return resultadoColumnas.get(columna);
	}
	
	public void cargarValor(int fila, int columna, int valor)//Matriz principal
	{
		noExcederDe(tamano, fila);
		noNegativos(fila);
		if(fila<matriztablero.length)
			matriztablero[fila][columna]=valor;
		
	}
	
	private int generarNumeroNaturalRandom(int mayorA) 
	{
		return (int) (Math.random() * (this.tamano*5) + mayorA);
	}

	public int mostrarTamano()
	{
		return tamano;
	}

	public int valoresDeTablero(int fila,int columna)
	{
		noExcederDe(tamano, fila);
		noNegativos(fila);
		noExcederDe(tamano, columna);
		noNegativos(columna);
		return matriztablero[fila][columna];
	}
	
	public int[][] matrizresultado()
	{
		return matrizresultado;
	}

	public int mostrarTablerosCompletados() 
	{
		return this.tablerosCompletados;
	}
	
	public int mostrarTablerosACompletar()
	{
		return this.tablerosACompletar;
	}
	
	public int mostrarAciertosFaltantes() 
	{
		return mostrarTablerosACompletar()-mostrarTablerosCompletados();
	}
	
	public void sumarTableroAcierto() 
	{
		this.tablerosCompletados++;
	}
	
	public int getValorTableroResuelto(int fila, int columna) {
		noExcederDe(tamano, columna);
		noNegativos(columna);
		noExcederDe(tamano, fila);
		noNegativos(fila);
		return matrizresultado[fila][columna];
	}
	
	public void setDificultadMedia()
	{
		this.tamano=4;
		this.tablerosACompletar=this.tamano*this.tamano;
		this.tablerosCompletados=0;
	}
	
	public void setDificultadFacil()
	{
		this.tamano=3;
		this.tablerosACompletar=this.tamano*this.tamano;
		this.tablerosCompletados=0;
	}
	
	public void setDificultadDificil() 
	{
		this.tamano=5;
		this.tablerosACompletar=this.tamano*this.tamano;
		this.tablerosCompletados=0;
	}
	
	//Resultados
	private int[][] cargarMatrizResultado(int tamano)
	{
		int[][] m=new int[tamano][tamano];
        for (int i = 0; i < m.length; i++) { 
            for (int j = 0; j < m[i].length; j++) {
                m[i][j]=generarNumeroNaturalRandom(tamano);
            }
        }
        return m;	
	}
	
	private ArrayList<Integer> cargarResultadosFilas()
	{
		ArrayList<Integer> l=new ArrayList<Integer>();
		int acum=0;
        for (int i = 0; i < mostrarTamano(); i++)
        { 
            for (int j = 0; j < this.matrizresultado[i].length; j++) 
            {
                acum=acum+this.matrizresultado[i][j];
            }
            l.add(acum);
            acum=0;
        }
		return l;
	}
	
	private ArrayList<Integer> cargarResultadosColumnas()
	{
		ArrayList<Integer> l =new ArrayList<Integer>();
		int acum=0;
		
		for(int i=0;i<this.matrizresultado[0].length;i++)
		{
			for(int j=0; j<this.matrizresultado.length;j++)
			{
				acum=acum+this.matrizresultado[j][i];
			}
            l.add(acum);
            acum=0;
		}
		
		return l;
	}
	
	//Verificaciones
	public boolean dificultadMedia() 
	{
		return mostrarTamano()==4;
	}
	
	public boolean dificultadFacil() 
	{
		return mostrarTamano()==3;
	}
	
	public boolean dificultadDificil()
	{
		return mostrarTamano()==5;
	}
	
	public boolean verificarFila(int fila) {
		noExcederDe(tamano, fila);
		noNegativos(fila);
		int suma=0;
		int contador=0;
		for(int i=0; i<matriztablero.length;i++){
			if(valoresDeTablero(fila, i)>0) {
				contador=contador+1;
				suma=suma+valoresDeTablero(fila, i);
			}
		}
		return suma==getResultadoFila(fila)&&contador==this.tamano;
	}

	public boolean verificarColumna(int columna) {
		noExcederDe(tamano, columna);
		noNegativos(columna);
		int suma=0;
		int contador=0;
		for(int i=0;i<matriztablero[0].length;i++) {
			if(valoresDeTablero(i, columna)>0) {
				contador=contador+1;
				suma=suma+valoresDeTablero(i, columna);
			}
		}
		return suma==getResultadoColumna(columna)&&contador==this.tamano;
	}
	
	public boolean tablerocompletado() {
		boolean band=true;
		for(int i=0;i<this.tamano;i++) {
			band=band&&verificarFila(i)&&verificarColumna(i);
		}
		return band;
	}
	
	public boolean superaResultadoColumna(int columna) {
		noExcederDe(tamano, columna);
		noNegativos(columna);
		int suma=0;
		for(int i=0;i<matriztablero[0].length;i++) {
			suma=suma+matriztablero[i][columna];
		}
		return suma>getResultadoColumna(columna);
	}
	
	public boolean superaResultadoFila(int fila) {
		noExcederDe(tamano, fila);
		noNegativos(fila);
		int suma=0;
		for(int i=0; i<matriztablero.length;i++){
			suma=suma+matriztablero[fila][i];
		}
		return suma>getResultadoFila(fila);
	}
	
	public boolean completoTodosLosTableros() 
	{
		return mostrarTablerosCompletados()==mostrarTablerosACompletar();
	}
	
	//Continuar//reiniciar
	public void continuar() {
		sumarTableroAcierto();
		this.matriztablero=new int[tamano][tamano];
		this.matrizresultado=cargarMatrizResultado(tamano);
		this.resultadoColumnas=cargarResultadosColumnas();
		this.resultadosFilas=cargarResultadosFilas();
	}
	
	public void reiniciar() {
		this.matriztablero=new int[tamano][tamano];
		this.matrizresultado=cargarMatrizResultado(tamano);
		this.resultadoColumnas=cargarResultadosColumnas();
		this.resultadosFilas=cargarResultadosFilas();
		this.tablerosCompletados=0;
	}
	
	//Exceptions
	private void noNegativos(int valor) {
		if(valor<0)
			throw new RuntimeException("Valor negativo no valido");
	}
	
	private void noExcederDe(int max, int valor)
	{
		if(valor>max) 
			throw new RuntimeException("valor ingresado excedido");
	}
}
