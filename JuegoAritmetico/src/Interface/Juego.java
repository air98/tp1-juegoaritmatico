package Interface;

import javax.swing.JFrame;


import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import javax.swing.SwingConstants;
import Game.Inteligencia;
import javax.swing.JLabel;

import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JMenuBar;

public class Juego {

	private Inteligencia inteligencia;
	private JFrame frame;
	
	JMenuBar menuBar;
	JMenu mnDificultad;
	JButton btnDificultadFacil;
	JButton btnDificultadMedia;
	JButton btnDificultadDificil;
	JMenu mnTutos;
	JButton btnComoJugar;
	
	private JTextField inputF1C1;
	private JTextField inputF1C2;
	private JTextField inputF1C3;
	private JTextField inputF1C4;
	private JTextField inputF2C1;
	private JTextField inputF2C2;
	private JTextField inputF2C3;
	private JTextField inputF2C4;
	private JTextField inputF3C1;
	private JTextField inputF3C2;
	private JTextField inputF3C3;
	private JTextField inputF3C4;
	private JTextField inputF4C1;
	private JTextField inputF4C2;
	private JTextField inputF4C3;
	private JTextField inputF4C4;
	private JTextField inputF1C5;
	private JTextField inputF2C5;
	private JTextField inputF3C5;
	private JTextField inputF4C5;
	private JTextField inputF5C1;
	private JTextField inputF5C2;
	private JTextField inputF5C3;
	private JTextField inputF5C4;
	private JTextField inputF5C5;
	private JLabel resultadoFila2;
	private JLabel resultadoFila3;
	private JLabel resultadoFila4;
	private JLabel resultadoFila5;
	private JLabel resultadoColumna1;
	private JLabel resultadoColumna2;
	private JLabel resultadoColumna3;
	private JLabel resultadoColumna4;
	private JLabel resultadoColumna5;
	private Button buttonVerificar;
	private JLabel resultadoFila1;
	private JLabel imagenprofesor;
	private JTextPane mensaje;
	private Button buttonAbandonar;
	private Button buttonSeguirJugando;
	private Button buttonReiniciar;


	public Juego(Inteligencia intelig) {
		this.inteligencia=intelig;
		initialize();

	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Juego Aritmetico: Suma");
		frame.setBounds(100, 100, 733, 424);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);


		iniciarDiseño();
		limitarinput();
		accionesJuego();
		
	}
	
	private void iniciarDiseño() 
	{
		frame.getContentPane().setLayout(null);

		//MENU
		menuBar = new JMenuBar();
		menuBar.setBackground(new Color(240, 240, 240));
		menuBar.setBounds(0, 0, 717, 35);
		frame.getContentPane().add(menuBar);
		
		mnDificultad = new JMenu("Dificultad");
		menuBar.add(mnDificultad);
		
		btnDificultadFacil = new JButton("Facil   ");
		mnDificultad.add(btnDificultadFacil);
		
		btnDificultadMedia = new JButton("Media ");
		mnDificultad.add(btnDificultadMedia);
		
		btnDificultadDificil = new JButton("Dificil  ");
		mnDificultad.add(btnDificultadDificil);
		
		mnTutos = new JMenu("Tutoriales");
		menuBar.add(mnTutos);
		
		btnComoJugar = new JButton("Como jugar");
		mnTutos.add(btnComoJugar);
		
		//input
		inputF1C1=new JTextField();
		inputF1C1.setBounds(56, 102, 47, 41);
		inputF1C1.setHorizontalAlignment(SwingConstants.CENTER);
		inputF1C1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(inputF1C1);
		inputF1C1.setColumns(10);
		
		inputF1C2 = new JTextField();
		inputF1C2.setBounds(119, 102, 47, 41);
		inputF1C2.setHorizontalAlignment(SwingConstants.CENTER);
		inputF1C2.setColumns(10);
		frame.getContentPane().add(inputF1C2);
		
		inputF1C3 = new JTextField();
		inputF1C3.setBounds(176, 102, 47, 41);
		inputF1C3.setHorizontalAlignment(SwingConstants.CENTER);
		inputF1C3.setColumns(10);
		frame.getContentPane().add(inputF1C3);
		
		inputF1C4 = new JTextField();
		inputF1C4.setBounds(233, 102, 47, 41);
		inputF1C4.setHorizontalAlignment(SwingConstants.CENTER);
		inputF1C4.setColumns(10);
		frame.getContentPane().add(inputF1C4);
		
		inputF2C1 = new JTextField();
		inputF2C1.setBounds(56, 154, 47, 41);
		inputF2C1.setHorizontalAlignment(SwingConstants.CENTER);
		inputF2C1.setColumns(10);
		frame.getContentPane().add(inputF2C1);
		
		inputF2C2 = new JTextField();
		inputF2C2.setBounds(119, 154, 47, 41);
		inputF2C2.setHorizontalAlignment(SwingConstants.CENTER);
		inputF2C2.setColumns(10);
		frame.getContentPane().add(inputF2C2);
		
		inputF2C3 = new JTextField();
		inputF2C3.setBounds(176, 154, 47, 41);
		inputF2C3.setHorizontalAlignment(SwingConstants.CENTER);
		inputF2C3.setColumns(10);
		frame.getContentPane().add(inputF2C3);
		
		inputF2C4 = new JTextField();
		inputF2C4.setBounds(233, 154, 47, 41);
		inputF2C4.setHorizontalAlignment(SwingConstants.CENTER);
		inputF2C4.setColumns(10);
		frame.getContentPane().add(inputF2C4);
		
		inputF3C1 = new JTextField();
		inputF3C1.setBounds(56, 206, 47, 41);
		inputF3C1.setHorizontalAlignment(SwingConstants.CENTER);
		inputF3C1.setColumns(10);
		frame.getContentPane().add(inputF3C1);
		
		inputF3C2 = new JTextField();
		inputF3C2.setBounds(119, 206, 47, 41);
		inputF3C2.setHorizontalAlignment(SwingConstants.CENTER);
		inputF3C2.setColumns(10);
		frame.getContentPane().add(inputF3C2);
		
		inputF3C3 = new JTextField();
		inputF3C3.setBounds(176, 206, 47, 41);
		inputF3C3.setHorizontalAlignment(SwingConstants.CENTER);
		inputF3C3.setColumns(10);
		frame.getContentPane().add(inputF3C3);
		
		inputF3C4 = new JTextField();
		inputF3C4.setBounds(233, 206, 47, 41);
		inputF3C4.setHorizontalAlignment(SwingConstants.CENTER);
		inputF3C4.setColumns(10);
		frame.getContentPane().add(inputF3C4);
		
		inputF4C1 = new JTextField();
		inputF4C1.setBounds(56, 258, 47, 41);
		inputF4C1.setHorizontalAlignment(SwingConstants.CENTER);
		inputF4C1.setColumns(10);
		frame.getContentPane().add(inputF4C1);
		
		inputF4C2 = new JTextField();
		inputF4C2.setBounds(119, 258, 47, 41);
		inputF4C2.setHorizontalAlignment(SwingConstants.CENTER);
		inputF4C2.setColumns(10);
		frame.getContentPane().add(inputF4C2);
		
		inputF4C3 = new JTextField();
		inputF4C3.setBounds(176, 258, 47, 41);
		inputF4C3.setHorizontalAlignment(SwingConstants.CENTER);
		inputF4C3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		inputF4C3.setColumns(10);
		frame.getContentPane().add(inputF4C3);
		
		inputF4C4 = new JTextField();
		inputF4C4.setBounds(233, 258, 47, 41);
		inputF4C4.setHorizontalAlignment(SwingConstants.CENTER);
		inputF4C4.setColumns(10);
		frame.getContentPane().add(inputF4C4);
		
		inputF1C5 = new JTextField();
		inputF1C5.setHorizontalAlignment(SwingConstants.CENTER);
		inputF1C5.setColumns(10);
		inputF1C5.setBounds(290, 102, 47, 41);
		frame.getContentPane().add(inputF1C5);
		inputF1C5.setVisible(false);
		
		inputF2C5 = new JTextField();
		inputF2C5.setHorizontalAlignment(SwingConstants.CENTER);
		inputF2C5.setColumns(10);
		inputF2C5.setBounds(290, 154, 47, 41);
		frame.getContentPane().add(inputF2C5);
		inputF2C5.setVisible(false);
		
		inputF3C5 = new JTextField();
		inputF3C5.setHorizontalAlignment(SwingConstants.CENTER);
		inputF3C5.setColumns(10);
		inputF3C5.setBounds(290, 206, 47, 41);
		frame.getContentPane().add(inputF3C5);
		inputF3C5.setVisible(false);
		
		inputF4C5 = new JTextField();
		inputF4C5.setHorizontalAlignment(SwingConstants.CENTER);
		inputF4C5.setColumns(10);
		inputF4C5.setBounds(290, 258, 47, 41);
		frame.getContentPane().add(inputF4C5);
		inputF4C5.setVisible(false);
		
		inputF5C1 = new JTextField();
		inputF5C1.setHorizontalAlignment(SwingConstants.CENTER);
		inputF5C1.setColumns(10);
		inputF5C1.setBounds(56, 308, 47, 41);
		frame.getContentPane().add(inputF5C1);
		inputF5C1.setVisible(false);
		
		inputF5C2 = new JTextField();
		inputF5C2.setHorizontalAlignment(SwingConstants.CENTER);
		inputF5C2.setColumns(10);
		inputF5C2.setBounds(119, 308, 47, 41);
		frame.getContentPane().add(inputF5C2);
		inputF5C2.setVisible(false);
		
		inputF5C3 = new JTextField();
		inputF5C3.setHorizontalAlignment(SwingConstants.CENTER);
		inputF5C3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		inputF5C3.setColumns(10);
		inputF5C3.setBounds(176, 308, 47, 41);
		frame.getContentPane().add(inputF5C3);
		inputF5C3.setVisible(false);
		
		inputF5C4 = new JTextField();
		inputF5C4.setHorizontalAlignment(SwingConstants.CENTER);
		inputF5C4.setColumns(10);
		inputF5C4.setBounds(233, 308, 47, 41);
		frame.getContentPane().add(inputF5C4);
		inputF5C4.setVisible(false);
		
		inputF5C5 = new JTextField();
		inputF5C5.setHorizontalAlignment(SwingConstants.CENTER);
		inputF5C5.setColumns(10);
		inputF5C5.setBounds(290, 308, 47, 41);
		frame.getContentPane().add(inputF5C5);
		inputF5C5.setVisible(false);
		
		//resultados
		resultadoFila1 = new JLabel("");
		resultadoFila1.setBounds(10, 102, 47, 41);
		resultadoFila1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		resultadoFila1.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(resultadoFila1);
		
		resultadoFila2 = new JLabel("");
		resultadoFila2.setBounds(10, 152, 47, 41);
		resultadoFila2.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoFila2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoFila2);
		
		resultadoFila3 = new JLabel("");
		resultadoFila3.setBounds(10, 204, 47, 41);;
		resultadoFila3.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoFila3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoFila3);
		
		resultadoFila4 = new JLabel("");
		resultadoFila4.setBounds(10, 258, 47, 41);
		resultadoFila4.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoFila4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoFila4);
		
		resultadoFila5 = new JLabel("");
		resultadoFila5.setBounds(10, 308, 47, 41);
		resultadoFila5.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoFila5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoFila5);
		
		resultadoColumna1 = new JLabel("");
		resultadoColumna1.setBounds(56, 63, 47, 41);
		resultadoColumna1.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoColumna1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoColumna1);
		
		resultadoColumna2 =new JLabel("");
		resultadoColumna2.setBounds(119, 63, 47, 41);
		resultadoColumna2.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoColumna2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoColumna2);
		
		resultadoColumna3 = new JLabel("");
		resultadoColumna3.setBounds(176, 63, 47, 41);
		resultadoColumna3.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoColumna3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoColumna3);
		
		resultadoColumna4 = new JLabel("");
		resultadoColumna4.setBounds(233, 63, 47, 41);
		resultadoColumna4.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoColumna4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoColumna4);
		
		resultadoColumna5 = new JLabel("");
		resultadoColumna5.setBounds(290, 63, 47, 41);
		resultadoColumna5.setHorizontalAlignment(SwingConstants.CENTER);
		resultadoColumna5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(resultadoColumna5);
		
		//Cargar Resultados
		obtenerResultados();
		
		mensaje = new JTextPane();
		mensaje.setBounds(408, 61, 270, 48);
		mensaje.setEditable(false);
		mensaje.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mensaje.setText("Bienvenido, debes completar los "+Integer.toString((this.inteligencia.mostrarAciertosFaltantes())+1)+ " tableros respetando la suma entre columnas y filas. Suerte!");
		frame.getContentPane().add(mensaje);
		
		imagenprofesor = new JLabel("");
		imagenprofesor.setBounds(398, 50, 309, 250);
		imagenprofesor.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/profesor.png")));
		frame.getContentPane().add(imagenprofesor);
		
		buttonVerificar = new Button("Verificar");
		buttonVerificar.setBounds(408, 320, 120, 41);
		buttonVerificar.setForeground(new Color(255, 255, 255));
		buttonVerificar.setFont(new Font("Dialog", Font.PLAIN, 14));
		buttonVerificar.setBackground(new Color(34, 139, 34));
		frame.getContentPane().add(buttonVerificar);
		
		buttonSeguirJugando = new Button("Seguir Jugando");
		buttonSeguirJugando.setBounds(547, 320, 120, 41);
		buttonSeguirJugando.setForeground(new Color(255, 255, 255));
		buttonSeguirJugando.setFont(new Font("Dialog", Font.PLAIN, 14));
		buttonSeguirJugando.setBackground(new Color(34, 139, 34));
		frame.getContentPane().add(buttonSeguirJugando);
		buttonSeguirJugando.setVisible(false);
		
		buttonReiniciar = new Button("Reiniciar");
		buttonReiniciar.setBounds(547, 320, 120, 41);
		buttonReiniciar.setForeground(new Color(255, 255, 255));
		buttonReiniciar.setFont(new Font("Dialog", Font.PLAIN, 14));
		buttonReiniciar.setBackground(new Color(0, 0, 255));
		frame.getContentPane().add(buttonReiniciar);
		buttonReiniciar.setVisible(false);
		
		buttonAbandonar = new Button("Abandonar");
		buttonAbandonar.setBounds(547, 320, 120, 41);
		buttonAbandonar.setForeground(Color.WHITE);
		buttonAbandonar.setFont(new Font("Dialog", Font.PLAIN, 14));
		buttonAbandonar.setBackground(new Color(220, 20, 60));
		frame.getContentPane().add(buttonAbandonar);
		

			
	}
	
	private void limitarinput() {
		//Limite a que solo se puedan ingresar numeros 0 a 9, y solo dos cifras!
		limitarSoloNumero(inputF1C1);
		limitarSoloNumero(inputF1C2);
		limitarSoloNumero(inputF1C3);
		limitarSoloNumero(inputF1C4);
		limitarSoloNumero(inputF1C5);
		limitarSoloNumero(inputF2C1);
		limitarSoloNumero(inputF2C2);
		limitarSoloNumero(inputF2C3);
		limitarSoloNumero(inputF2C4);
		limitarSoloNumero(inputF2C5);
		limitarSoloNumero(inputF3C1);
		limitarSoloNumero(inputF3C2);
		limitarSoloNumero(inputF3C3);
		limitarSoloNumero(inputF3C4);
		limitarSoloNumero(inputF4C5);
		limitarSoloNumero(inputF4C1);
		limitarSoloNumero(inputF4C2);
		limitarSoloNumero(inputF4C3);
		limitarSoloNumero(inputF4C4);
		limitarSoloNumero(inputF4C5);
		limitarSoloNumero(inputF5C1);
		limitarSoloNumero(inputF5C2);
		limitarSoloNumero(inputF5C3);
		limitarSoloNumero(inputF5C4);
		limitarSoloNumero(inputF5C5);
	}
	
	private void accionesJuego() 
	{
		//Abandonar
		buttonAbandonar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			cargarTableroResuelto();
			buttonAbandonar.setVisible(false);
			buttonVerificar.setVisible(false);
			noInput();
			buttonReiniciar.setVisible(true);
			mensaje.setText("Lamento que hayas abandonado, te dejo aqui una posible solucion. Espero volver a verte pronto!");
			}
		});

		//Verificar
		buttonVerificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//verificaciones
				verificarCasillaXcasilla();
				//Finalizo tablero correctamente
				siFinalizoTablero();
		}
		});
		
		//Boton seguir jugando en caso de que termine el tablero
		buttonSeguirJugando.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				continuarJuego();
			}
			});
		
		//Boton reiniciar, en caso que el jugador quiera seguir jugando
		buttonReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			reiniciarJuego();
			}
		});
		
		//MENU
		this.btnDificultadFacil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDificultadFacil();
				reiniciarJuego();
			}
		});
		
		this.btnDificultadMedia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDificultadMedia();
				reiniciarJuego();
			}
		});
		
		this.btnDificultadDificil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDificultadDificil();
				reiniciarJuego();
			}
		});
		
		this.btnComoJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        try
		        {	
		        	if(Desktop.isDesktopSupported())
		            {
		        		Desktop desktop = Desktop.getDesktop();
		        		File file = new File("src\\pagina.html");
		        		if(file.exists())
			                desktop.open(file);
		            }
		            
		        }
		        catch(Exception e1)
		        {
		            e1.printStackTrace();
		        }
			}
		});
	}
	

	//Funciones auxiliares
	private void setDificultadFacil()
	{
		inteligencia.setDificultadFacil();
		inputF4C1.setVisible(false);
		inputF4C2.setVisible(false);
		inputF4C3.setVisible(false);
		inputF4C4.setVisible(false);
		inputF1C4.setVisible(false);
		inputF2C4.setVisible(false);
		inputF3C4.setVisible(false);
		inputF4C4.setVisible(false);
		inputF1C5.setVisible(false);
		inputF2C5.setVisible(false);
		inputF3C5.setVisible(false);
		inputF4C5.setVisible(false);
		inputF5C1.setVisible(false);
		inputF5C2.setVisible(false);
		inputF5C3.setVisible(false);
		inputF5C4.setVisible(false);
		inputF5C5.setVisible(false);
		resultadoColumna4.setVisible(false);
		resultadoColumna5.setVisible(false);
		resultadoFila4.setVisible(false);
		resultadoFila5.setVisible(false);
	}
	
	private void setDificultadMedia() 
	{
		inteligencia.setDificultadMedia();
		inputF4C1.setVisible(true);
		inputF4C2.setVisible(true);
		inputF4C3.setVisible(true);
		inputF4C4.setVisible(true);
		inputF1C4.setVisible(true);
		inputF2C4.setVisible(true);
		inputF3C4.setVisible(true);
		inputF4C4.setVisible(true);
		inputF1C5.setVisible(false);
		inputF2C5.setVisible(false);
		inputF3C5.setVisible(false);
		inputF4C5.setVisible(false);
		inputF5C1.setVisible(false);
		inputF5C2.setVisible(false);
		inputF5C3.setVisible(false);
		inputF5C4.setVisible(false);
		inputF5C5.setVisible(false);
		resultadoColumna4.setVisible(true);
		resultadoFila4.setVisible(true);
		resultadoColumna5.setVisible(false);
		resultadoFila5.setVisible(false);
	}
	
	private void setDificultadDificil()
	{
		inteligencia.setDificultadDificil();
		inputF4C1.setVisible(true);
		inputF4C2.setVisible(true);
		inputF4C3.setVisible(true);
		inputF4C4.setVisible(true);
		inputF1C4.setVisible(true);
		inputF2C4.setVisible(true);
		inputF3C4.setVisible(true);
		inputF4C4.setVisible(true);
		inputF1C5.setVisible(true);
		inputF2C5.setVisible(true);
		inputF3C5.setVisible(true);
		inputF4C5.setVisible(true);
		inputF5C1.setVisible(true);
		inputF5C2.setVisible(true);
		inputF5C3.setVisible(true);
		inputF5C4.setVisible(true);
		inputF5C5.setVisible(true);
		resultadoColumna4.setVisible(true);
		resultadoFila4.setVisible(true);
		resultadoColumna5.setVisible(true);
		resultadoFila5.setVisible(true);
	}
	
	private void verificarcasilla(JTextField casilla, int fila, int columna) 
	{
		//el numero esta dentro delos rangos
		if(traducirInt(casilla)<resultadoFila(fila)&&traducirInt(casilla)<resultadoColumna(columna))
		{
			if(cambio(casilla, obtenerValorTablero(fila,columna)))//Solo si cambio el valor verificamos
			{
				String mensaje=" "+"Bien hecho, sigue asi! Queda camino por recorrer.";
				Juego.this.mensaje.setText(mensaje);
				guardarResultado(casilla, fila, columna);
			//la suma no supera
			if((superaLaSumaColumna(columna)||superaLaSumaFila(fila))) 
			{
				String mensaje1=" "+"Debes verificar las sumas, animo tu puedes.";
				Juego.this.mensaje.setText(mensaje1);
				casilla.setText("");
				reiniciarCasilla(fila, columna);
			}
			}
		}
		else
		{
			String mensaje=" "+"Algo no cuadra, ese numero esta fuera de rango!.";
			Juego.this.mensaje.setText(mensaje);
			casilla.setText("");
			reiniciarCasilla(fila, columna);
		}
	}
	
	private void verificarCasillaXcasilla()
	{
		verificarcasilla(inputF1C1, 0, 0);
		verificarcasilla(inputF1C2, 0, 1);
		verificarcasilla(inputF1C3, 0, 2);
		verificarcasilla(inputF2C1, 1, 0);
		verificarcasilla(inputF2C2, 1, 1);
		verificarcasilla(inputF2C3, 1, 2);
		verificarcasilla(inputF3C1, 2, 0);
		verificarcasilla(inputF3C2, 2, 1);
		verificarcasilla(inputF3C3, 2, 2);
		if(inteligencia.dificultadMedia()||inteligencia.dificultadDificil()){
			verificarcasilla(inputF2C4, 1, 3);
			verificarcasilla(inputF1C4, 0, 3);
			verificarcasilla(inputF3C4, 2, 3);
			verificarcasilla(inputF4C1, 3, 0);
			verificarcasilla(inputF4C2, 3, 1);
			verificarcasilla(inputF4C3, 3, 2);
			verificarcasilla(inputF4C4, 3, 3);
		}
		if(inteligencia.dificultadDificil()) {
			verificarcasilla(inputF1C5, 0, 4);
			verificarcasilla(inputF2C5, 1, 4);
			verificarcasilla(inputF3C5, 2, 4);
			verificarcasilla(inputF4C5, 3, 4);
			verificarcasilla(inputF5C1, 4, 0);
			verificarcasilla(inputF5C2, 4, 1);
			verificarcasilla(inputF5C3, 4, 2);
			verificarcasilla(inputF5C4, 4, 3);
			verificarcasilla(inputF5C5, 4, 4);
			
		}
	}
	
	private void siFinalizoTablero()
	{
		if(inteligencia.tablerocompletado())
		{
			//si llego al final
			if(inteligencia.completoTodosLosTableros()) {
				mensaje.setText("Felicitaciones, terminaste todos los tableros a los cuales fuiste retado, puedes volver a jugar cuando gustes!" );
				buttonReiniciar.setVisible(true);
				buttonAbandonar.setVisible(false);
				buttonVerificar.setVisible(false);
				noInput();
				
			}
			else
			{
				Juego.this.mensaje.setText("Finalizaste el tablero, pero aun quedan mas por resolver!");
				buttonAbandonar.setVisible(false);
				buttonVerificar.setVisible(false);
				noInput();
				buttonSeguirJugando.setVisible(true);
			}
		}
	}
	
	private int obtenerValorTablero(int fila, int columna)
	{
		return inteligencia.valoresDeTablero(fila,columna);
	}
	
	private int resultadoFila(int fila)
	{
		return inteligencia.getResultadoFila(fila);
	}
	
	private int resultadoColumna(int columna)
	{
		return inteligencia.getResultadoColumna(columna);
	}

	private boolean superaLaSumaFila(int fila) 
	{
		return inteligencia.superaResultadoFila(fila);
	}

	private boolean superaLaSumaColumna(int columna)
	{
		return inteligencia.superaResultadoColumna(columna);
	}

	private boolean cambio(JTextField valor, int v) 
	{
		return traducirInt(valor)!=v;
	}

	private void guardarResultado(JTextField valor, int fila, int columna)
	{
		int v=traducirInt(valor);
		inteligencia.cargarValor(fila, columna, v);
	}

	private void reiniciarCasilla(int fila, int columna) 
	{
		inteligencia.cargarValor(fila, columna, 0);
	}
	
	private int traducirInt(JTextField text)
	{
		if(text.getText().length()>0) 
			return Integer.parseInt(text.getText());
		return 0;//si no puso nada 
	}
	
	private void reiniciarJuego() 
	{
		inteligencia.reiniciar();
		mensaje.setText("Bienvenido, debes completar el tablero respetando la suma entre columnas y filas, tienes: " + Integer.toString((inteligencia.mostrarAciertosFaltantes()+1))+ " tableros por completar" );
		buttonAbandonar.setVisible(true);
		buttonVerificar.setVisible(true);
		buttonSeguirJugando.setVisible(false);
		buttonReiniciar.setVisible(false);
		habilitarInput();
		ponerTodasLasCasillasEnVacio();
		obtenerResultados();	
	}
	
	private void continuarJuego() 
	{
		inteligencia.continuar();
		mensaje.setText("Debes completar el tablero respetando la suma entre columnas y filas, te quedan: " + Integer.toString((inteligencia.mostrarAciertosFaltantes()+1))+ " tableros por completar" );
		buttonAbandonar.setVisible(true);
		buttonVerificar.setVisible(true);
		buttonSeguirJugando.setVisible(false);
		habilitarInput();
		ponerTodasLasCasillasEnVacio();
		obtenerResultados();
	}
	
	private void noInput()
	{
		inputF1C1.setEditable(false);
		inputF1C2.setEditable(false);
		inputF1C3.setEditable(false);
		inputF1C4.setEditable(false);
		inputF1C5.setEditable(false);
		inputF2C1.setEditable(false);
		inputF2C2.setEditable(false);
		inputF2C3.setEditable(false);
		inputF2C4.setEditable(false);
		inputF2C5.setEditable(false);
		inputF3C1.setEditable(false);
		inputF3C2.setEditable(false);
		inputF3C3.setEditable(false);
		inputF3C4.setEditable(false);
		inputF3C5.setEditable(false);
		inputF4C1.setEditable(false);
		inputF4C2.setEditable(false);
		inputF4C3.setEditable(false);
		inputF4C4.setEditable(false);
		inputF4C5.setEditable(false);
		inputF5C1.setEditable(false);
		inputF5C2.setEditable(false);
		inputF5C3.setEditable(false);
		inputF5C4.setEditable(false);
		inputF5C5.setEditable(false);
	}
	
	private void habilitarInput()
	{
		inputF1C1.setEditable(true);
		inputF1C2.setEditable(true);
		inputF1C3.setEditable(true);
		inputF1C4.setEditable(true);
		inputF1C5.setEditable(true);
		inputF2C1.setEditable(true);
		inputF2C2.setEditable(true);
		inputF2C3.setEditable(true);
		inputF2C4.setEditable(true);
		inputF2C5.setEditable(true);
		inputF3C1.setEditable(true);
		inputF3C2.setEditable(true);
		inputF3C3.setEditable(true);
		inputF3C4.setEditable(true);
		inputF3C5.setEditable(true);
		inputF4C1.setEditable(true);
		inputF4C2.setEditable(true);
		inputF4C3.setEditable(true);
		inputF4C4.setEditable(true);
		inputF4C5.setEditable(true);
		inputF5C1.setEditable(true);
		inputF5C2.setEditable(true);
		inputF5C3.setEditable(true);
		inputF5C4.setEditable(true);
		inputF5C5.setEditable(true);
	}
	
	private void obtenerResultados()
	{
		resultadoFila1.setText(Integer.toString(resultadoFila(0)));
		resultadoFila2.setText(Integer.toString(resultadoFila(1)));
		resultadoFila3.setText(Integer.toString(resultadoFila(2)));
		resultadoColumna1.setText(Integer.toString(resultadoColumna(0)));
		resultadoColumna2.setText(Integer.toString(resultadoColumna(1)));
		resultadoColumna3.setText(Integer.toString(resultadoColumna(2)));
		if(inteligencia.dificultadMedia()||inteligencia.dificultadDificil()) {
			resultadoFila4.setText(Integer.toString(resultadoFila(3)));
			resultadoColumna4.setText(Integer.toString(resultadoColumna(3)));
		}
		if(inteligencia.dificultadDificil()) {
			resultadoFila5.setText(Integer.toString(resultadoFila(4)));
			resultadoColumna5.setText(Integer.toString(resultadoColumna(4)));
		}
	}
	
	private void ponerTodasLasCasillasEnVacio()
	{
		inputF1C1.setText("");
		inputF1C2.setText("");
		inputF1C3.setText("");
		inputF1C4.setText("");
		inputF1C5.setText("");
		inputF2C1.setText("");
		inputF2C2.setText("");
		inputF2C3.setText("");
		inputF2C4.setText("");
		inputF2C5.setText("");
		inputF3C1.setText("");
		inputF3C2.setText("");
		inputF3C3.setText("");
		inputF3C4.setText("");
		inputF3C5.setText("");
		inputF4C1.setText("");
		inputF4C2.setText("");
		inputF4C3.setText("");
		inputF4C4.setText("");
		inputF4C5.setText("");
		inputF5C1.setText("");
		inputF5C2.setText("");
		inputF5C3.setText("");
		inputF5C4.setText("");
		inputF5C5.setText("");
	}
	
	private void cargarTableroResuelto()
	{
		inputF1C1.setText(Integer.toString(inteligencia.getValorTableroResuelto(0, 0)));
		inputF1C2.setText(Integer.toString(inteligencia.getValorTableroResuelto(0, 1)));
		inputF1C3.setText(Integer.toString(inteligencia.getValorTableroResuelto(0, 2)));
		
		inputF2C1.setText(Integer.toString(inteligencia.getValorTableroResuelto(1, 0)));
		inputF2C2.setText(Integer.toString(inteligencia.getValorTableroResuelto(1, 1)));
		inputF2C3.setText(Integer.toString(inteligencia.getValorTableroResuelto(1, 2)));
		
		inputF3C1.setText(Integer.toString(inteligencia.getValorTableroResuelto(2, 0)));
		inputF3C2.setText(Integer.toString(inteligencia.getValorTableroResuelto(2, 1)));
		inputF3C3.setText(Integer.toString(inteligencia.getValorTableroResuelto(2, 2)));
		
		if(inteligencia.dificultadMedia()||inteligencia.dificultadDificil()) 
		{
			inputF2C4.setText(Integer.toString(inteligencia.getValorTableroResuelto(1, 3)));
			inputF1C4.setText(Integer.toString(inteligencia.getValorTableroResuelto(0, 3)));
			inputF3C4.setText(Integer.toString(inteligencia.getValorTableroResuelto(2, 3)));
			inputF4C1.setText(Integer.toString(inteligencia.getValorTableroResuelto(3, 0)));
			inputF4C2.setText(Integer.toString(inteligencia.getValorTableroResuelto(3, 1)));
			inputF4C3.setText(Integer.toString(inteligencia.getValorTableroResuelto(3, 2)));
			inputF4C4.setText(Integer.toString(inteligencia.getValorTableroResuelto(3, 3)));
		}
		if(inteligencia.dificultadDificil()) 
		{
			inputF5C1.setText(Integer.toString(inteligencia.getValorTableroResuelto(4, 0)));
			inputF5C2.setText(Integer.toString(inteligencia.getValorTableroResuelto(4, 1)));
			inputF5C3.setText(Integer.toString(inteligencia.getValorTableroResuelto(4, 2)));
			inputF5C4.setText(Integer.toString(inteligencia.getValorTableroResuelto(4, 3)));
			inputF5C5.setText(Integer.toString(inteligencia.getValorTableroResuelto(4, 4)));
			inputF1C5.setText(Integer.toString(inteligencia.getValorTableroResuelto(0, 4)));
			inputF2C5.setText(Integer.toString(inteligencia.getValorTableroResuelto(1, 4)));
			inputF3C5.setText(Integer.toString(inteligencia.getValorTableroResuelto(2, 4)));
			inputF4C5.setText(Integer.toString(inteligencia.getValorTableroResuelto(3, 4)));




		}
	}
	
	private void limitarSoloNumero(JTextField casillero) 
	{
		casillero.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e)
			{
				String t=casillero.getText();
				char c=e.getKeyChar();
				if((c<'0'||c>'9'))//SOLO NUMEROS;
				{
					e.consume();
				}
				if(t.length()>=2)//DOS CIFRAS
				{
					e.consume();
				}
			}
		});
	}
	
	public void visible(boolean b) {
		frame.setVisible(b);
	}
}
