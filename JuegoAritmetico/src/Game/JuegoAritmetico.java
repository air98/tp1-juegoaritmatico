package Game;

import java.awt.EventQueue;
import Interface.Juego;

public class JuegoAritmetico {

	public JuegoAritmetico() {
		
	}

	public static void main(String[] args)
	{
			EventQueue.invokeLater(new Runnable()
			{
			public void run()
			{
				try
				{
					Inteligencia inteligencia=new Inteligencia();
					Juego game= new Juego(inteligencia);
					game.visible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});	
		
	}
}
